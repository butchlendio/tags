const express = require("express")
const router = express.Router();

const{
    selectAllTagsController,
    selectTagsController,
    searchTagsController,
    updateTagsController
}=require("../src/controllers/transaction/app");

const makeExpressCallback = require("../src/express-callback/app");

//view all tags
router.get("/view", makeExpressCallback(selectAllTagsController))

//view tags by user
router.get("/view/:id", makeExpressCallback(selectTagsController))

//search by tags
router.get("/search/tags", makeExpressCallback(searchTagsController))

//update tag
router.put("/update/:id", makeExpressCallback(updateTagsController))
module.exports = router;