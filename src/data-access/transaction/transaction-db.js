const db = ({dbs}) =>{
    return Object.freeze({
        selectAllTags,
        selectAllTagsByUser,
        searchTags,
        validateUser,
        updateTags

    });

    async function selectAllTags(){
        const db = await dbs();
        const sql = `SELECT t.*,u."userName",v."title" FROM tags t 
                        LEFT JOIN videos v ON v."id"=t."video_id"
                        LEFT JOIN users u ON u."id"=t."created_by"`
        return db.query(sql)
    }

    async function selectAllTagsByUser(id){
        const db = await dbs();
        const sql = `SELECT t.*,u."userName",v."title" FROM tags t 
        LEFT JOIN videos v ON v."id"=t."video_id"
        LEFT JOIN users u ON u."id"=t."created_by"
        WHERE u."id"=${id} ORDER BY t."desc" asc`
        return db.query(sql)
    }

    async function searchTags(info){
        const db = await dbs();
        const sql = `
        SELECT t.*,u."userName",v."title" FROM tags t 
        LEFT JOIN videos v ON v."id"=t."video_id"
        LEFT JOIN users u ON u."id"=t."created_by"
        WHERE t."desc" LIKE '${info}%';`
        return db.query(sql)
    }

    async function validateUser(data){
        const db = await dbs();
        const sql = `
        SELECT * FROM tags WHERE "id"=${data.id} AND "created_by"=${data.info.user}`
        return db.query(sql)
    }

    async function updateTags(id,info){
        const db = await dbs();
        const sql = `
        UPDATE tags set "desc"='${info.desc}' 
        WHERE "id"=${id} AND "created_by"=${info.created_by}`
        return db.query(sql)
    }

};

module.exports = db;    