const db = require("./transaction-db");
const makeDb = require("../db");

const TransDb = makeDb({db});

module.exports = TransDb