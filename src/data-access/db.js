const dotenv = require("dotenv");
const pg = require("pg");
dotenv.config();

const config = {
  user: process.env.PGUSER,
  database: process.env.PGDATABASE,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT,
  host: process.env.PGHOST,
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000
};


const pool = new pg.Pool(config);

async function dbs() {
  try {
    return pool;
  } catch (e) {
    pool.end(); // end connection
    console.log("Errors: ", e);
  }
}

const makeDb = ({ db }) => {
  return db({ dbs });
};

module.exports = makeDb;
