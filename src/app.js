const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
dotenv.config();
const app = express();


app.use(cors());

// Body Parser middleware to handle raw JSON files
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// transactions routes
app.use("/api", require("../routes/transactions")); 

// // action routes
// app.use("/aces/actions", require("../routes/api/actions"));

// // access-rights routes
// app.use("/aces/access-rights", require("../routes/api/access-rights"));

// // roles routes
// app.use("/aces/roles",  require("../routes/api/roles"));



const PORT = process.env.PORT ;

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}.`);
});

console.log(`Server is connecting to db: ${process.env.PGDATABASE}.`);

module.exports = app;
