
const {
    selectAllTagsUseCase,
    selectTagsUseCase,
    searchTagsUseCase,
    updateTagsUseCase
} = require("../../user-cases/transaction/app")

const selectAllTags = require("./select-all-tags")
const selectTags = require("./select-all-tags-by-user")
const searchTags = require("./search-tags")
const updateTags = require("./update-tags")

const selectAllTagsController = selectAllTags({selectAllTagsUseCase})
const selectTagsController = selectTags({selectTagsUseCase})
const searchTagsController = searchTags({searchTagsUseCase})
const updateTagsController = updateTags({updateTagsUseCase})


const controller = Object.freeze({
    selectAllTagsController,
    selectTagsController,
    searchTagsController,
    updateTagsController
})

module.exports = controller;

module.exports = {
    selectAllTagsController,
    selectTagsController,
    searchTagsController,
    updateTagsController
};