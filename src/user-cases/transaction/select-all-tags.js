const selectAllTags = ({TransDb}) => {
    return async function selectAll(){
        const result = await TransDb.selectAllTags();

        return result.rows
    }
}

module.exports = selectAllTags