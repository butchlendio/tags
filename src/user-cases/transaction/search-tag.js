const searchTags = ({TransDb}) => {
    return async function selectAll(info){
        const result = await TransDb.searchTags(info.tagsName);

        return result.rows
    }
}

module.exports = searchTags