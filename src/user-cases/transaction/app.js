const TransDb = require("../../data-access/transaction/app");

const selectAllTags = require("./select-all-tags");
const selectTags = require("./select-all-tags-by-user")
const searchTags = require("./search-tag")
const updateTags = require("./update-tag")

const selectAllTagsUseCase = selectAllTags({TransDb})
const selectTagsUseCase = selectTags({TransDb})
const searchTagsUseCase = searchTags({TransDb})
const updateTagsUseCase = updateTags({TransDb})

const services = Object.freeze({
    selectAllTagsUseCase,
    selectTagsUseCase,
    searchTagsUseCase,
    updateTagsUseCase
})

module.exports = services

module.exports = {
    selectAllTagsUseCase,
    selectTagsUseCase,
    searchTagsUseCase,
    updateTagsUseCase
}