const selectTags = ({TransDb}) => {
    return async function selectAll(info){
        
        const result = await TransDb.selectAllTagsByUser(info.id);

        return result.rows
    }
}

module.exports = selectTags